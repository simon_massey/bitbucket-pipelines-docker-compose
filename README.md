# Using docker-compose on Bitbucket Pipelines

[docker-compose](https://docs.docker.com/compose/) is a python
application and we can install it via
[pip](https://pip.pypa.io/en/stable/).

We use [Docker in Docker](https://hub.docker.com/_/docker/) as
_custom_ image in our build.

We can issue `docker-compose [...]` commands without any issue.

## Example build

This example uses 2 containers a _server_ and a _client_ (reusing the
same image).  The server takes some time to start, so the client waits
to be ready.

## How does this work?

1. The `bitbucket-pipelines.yml` pulls the things necessary to run `docker-compose run -T client ...`
1. In `docker-compose.yaml` the `client` has a `volume: ./src:/usr/local/src` that will mount the code that the pipeline checked out _over_ anything inside of the built `client` container
1. The `docker-compose run -T client` will run `test.sh` from the latest checkout in the `client`. This actually needs to build and test the code inside the container as it has a clean checkout. The demo code simple checks that mysql is running as a sibling. A typical script would "install, make, test". 
1. If you have your script write an xUnit XML result into a folder 'test-results' then the pipeline will scan for such a folder and fail the build if the xml says that there were test failures. 